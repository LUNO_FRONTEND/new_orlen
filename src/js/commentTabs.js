$(".item-humen").click(function () {
  var id = $(this).attr("data-tab"),
    content = $('.tabs__block[data-tab="' + id + '"]');
  $(".item-humen.active").removeClass("active");
  $(this).addClass("active");

  $(".item-comment.active").removeClass("active");
  content.addClass("active");
});
